import re
import sqlite3
from datetime import date

import bs4
import pandas as pd
import requests
from tqdm import tqdm

link_podstawa = 'https://gratka.pl/nieruchomosci/mieszkania/lodz/sprzedaz'
link_strona = '?page='
link_sort = '&sort=newest'

today = date.today()

conn = sqlite3.connect('gratka_db.db')
c = conn.cursor()

wszystkie_dane_mieszkan = []
lista_rynkow = ['wtorny', 'pierwotny']
for rynek in lista_rynkow:
    linki = []
    print('Scrapujemy rynek ' + str(rynek))
    liczba_stron = bs4.BeautifulSoup(requests.get(link_podstawa + '?rynek=' + rynek).text, "html.parser").body.find(
        'div', {
            'class': 'pagination'}).find('input', {'class': 'pagination__input'}).get('max')
    nrStrony = 1
    pbar = tqdm(total=int(liczba_stron), position=0)
    while nrStrony <= int(liczba_stron):
        strona = requests.get(link_podstawa + link_strona + str(nrStrony) + str(rynek) + link_sort)
        body = bs4.BeautifulSoup(strona.text, "html.parser").body
        zero = 0

        while zero < len(body.find_all('article', {'class': 'teaserEstate'})):
            strona_mieszkania = {
                'link': body.find_all('article', {'class': 'teaserEstate'})[int(zero)].get('data-href'),
                'data_aktualizacji': re.search('\d{2}.\d{2}.\d{4}',
                                               body.find_all('article', {'class': 'teaserEstate'})[1].find_all('li', {
                                                   'class': 'teaserEstate__info'})[0].text).group()}
            linki.append(strona_mieszkania)
            zero = zero + 1
        nrStrony = nrStrony + 1
        pbar.update(1)
    print('')
    print('Scrapujemy mieszkania')

    pbar = tqdm(total=len(linki), position=0)
    for mieszkanie in linki:

        data_aktualizacji = mieszkanie.get('data_aktualizacji')
        link_do_mieszkania = mieszkanie.get('link')
        powierzchnia, liczba_pokoi, pietro, forma_wlasnosci, stan, typ_zabudowy, liczba_pieter_budynku, powierzchnia_dodatkowa = '', '', '', '', '', '', '', ''
        okna, usytuowanie, cena, cena_za_metr, miasto, dzielnica, wojewodztwo, miejsca_parkingowe, komunikacja, dostepnosc = '', '', '', '', '', '', '', '', '', ''
        glosnosc, stan_instalacji, material_budynku, oplaty, forma_kuchni, pozostale, edukacja, zdrowie_i_uroda, rok_budowy = '', '', '', '', '', '', '', '', ''
        lista_telefonow = []
        strona = requests.get(link_do_mieszkania)
        body = bs4.BeautifulSoup(strona.text, 'html.parser').body
        scripts = body.find_all('script')
        rynek_check = ''
        for script in scripts:
            if 'rynek' in script.text:
                rynek_check = script.text
        if rynek_check.find(rynek) > 0:
            parametry = body.find('ul', {'class': 'parameters__rolled'})
            try:
                nazwa_oferty = body.find('h1', {'class': 'sticker__title'}).text
            except:
                nazwa_oferty = ''
            try:
                for telefon in parametry.find_all('a', {'class': 'phoneButton__button'}):
                    lista_telefonow.append(telefon.get('data-full-phone-number'))
                if len(lista_telefonow) == 1:
                    lista_telefonow.append(str(''))
                elif len(lista_telefonow) == 0:
                    lista_telefonow.append(str(''))
                    lista_telefonow.append(str(''))
                else:
                    pass
            except:
                lista_telefonow = ['', '']
            try:
                cena = str(
                    body.find('span', {'class': 'priceInfo__value'}).text.split('\n')[1].replace(' ', ''))
            except:
                cena = ''

            try:
                cena_za_metr = body.find('span', {'class': 'priceInfo__additional'}).text.split('\n')[1].replace(' m2',
                                                                                                                 '')
            except:
                cena_za_metr = ''

            zero = 0
            try:
                parametry_len = len(parametry.find_all('li'))
            except:
                parametry_len = 0
            while zero < parametry_len:
                try:
                    atrybut = parametry.find_all('li')[zero].text.split('\n')[1]
                    wartosc_atrybutu = parametry.find_all('li')[zero].find('b', {'class': 'parameters__value'}).text

                    if atrybut == 'Powierzchnia w m2':
                        powierzchnia = wartosc_atrybutu
                    elif atrybut == 'Liczba pokoi':
                        liczba_pokoi = wartosc_atrybutu
                    elif atrybut == 'Piętro':
                        pietro = wartosc_atrybutu
                    elif atrybut == 'Forma własności':
                        forma_wlasnosci = wartosc_atrybutu
                    elif atrybut == 'Stan':
                        stan = wartosc_atrybutu
                    elif atrybut == 'Typ zabudowy':
                        typ_zabudowy = wartosc_atrybutu
                    elif atrybut == 'Liczba pięter w budynku':
                        liczba_pieter_budynku = wartosc_atrybutu
                    elif atrybut == 'Miejsce parkingowe':
                        miejsca_parkingowe = wartosc_atrybutu
                    elif atrybut == 'Okna':
                        okna = wartosc_atrybutu
                    elif atrybut == 'Usytuowanie względem stron świata':
                        usytuowanie = wartosc_atrybutu
                    elif atrybut == 'Głośność':
                        glosnosc = wartosc_atrybutu
                    elif atrybut == 'Komunikacja':
                        komunikacja = wartosc_atrybutu
                    elif atrybut == 'Zdrowie i uroda':
                        zdrowie_i_uroda = wartosc_atrybutu
                    elif atrybut == 'Edukacja':
                        edukacja = wartosc_atrybutu
                    elif atrybut == 'Pozostałe':
                        pozostale = wartosc_atrybutu
                    elif atrybut == 'Forma kuchni':
                        forma_kuchni = wartosc_atrybutu
                    elif atrybut == 'Opłaty (czynsz administracyjny, media)':
                        oplaty = wartosc_atrybutu
                    elif atrybut == 'Materiał budynku':
                        material_budynku = wartosc_atrybutu
                    elif atrybut == 'Miejsce parkingowe':
                        miejsca_parkingowe = wartosc_atrybutu
                    elif atrybut == 'Stan instalacji':
                        stan_instalacji = wartosc_atrybutu
                    elif atrybut == 'Lokalizacja':
                        miasto = body.find('ul', {'class': 'parameters__rolled'}).find_all('li')[zero].find_all('a', {
                            'class': 'parameters__locationLink'})[0].text
                        dzielnica = \
                            body.find('ul', {'class': 'parameters__rolled'}).find_all('li')[zero].find_all('a', {
                                'class': 'parameters__locationLink'})[1].text
                        wojewodztwo = \
                            body.find('ul', {'class': 'parameters__rolled'}).find_all('li')[zero].find_all('a', {
                                'class': 'parameters__locationLink'})[2].text
                    elif atrybut == 'Rok budowy':
                        rok_budowy = wartosc_atrybutu
                    elif atrybut == 'Dostępność od':
                        dostepnosc = wartosc_atrybutu
                    elif atrybut == 'Powierzchnia dodatkowa':
                        powierzchnia_dodatkowa = wartosc_atrybutu
                    else:
                        pass
                except:
                    pass

                zero = zero + 1

            dane_mieszkania = {'ID': re.search('\d{3,10}$', link_do_mieszkania).group(),
                               'Nazwa oferty': nazwa_oferty,
                               'Lokalizacja - województwo': wojewodztwo,
                               'Lokalizacja - miasto': miasto,
                               'Lokalizacja - dzielnica': dzielnica,
                               'Cena': cena.replace(' zł', '').replace(' ', ''),
                               'Cena za metr': cena_za_metr.replace('zł/m2', '').replace(' ', ''),
                               'Opłaty': oplaty.replace(' zł', '').replace(' ', ''),
                               'Powierzchnia w m2': powierzchnia.replace(' m2', '').replace(' ', ''),
                               'Nr telefonu 1': lista_telefonow[0],
                               'Nr telefonu 2': lista_telefonow[1],
                               'Piętro': pietro,
                               'Liczba pięter w budynku': liczba_pieter_budynku,
                               'Liczba pokoi': liczba_pokoi,
                               'Forma kuchni': forma_kuchni,
                               'Forma własności': forma_wlasnosci,
                               'Stan': stan,
                               'Stan instalacji': stan_instalacji,
                               'Typ zabudowy': typ_zabudowy,
                               'Materiał budynku': material_budynku,
                               'Okna': okna,
                               'Miejsca parkingowe': miejsca_parkingowe,
                               'Usytuowanie (strony świata)': usytuowanie,
                               'Komunikacja': komunikacja,
                               'Zdrowie i uroda': zdrowie_i_uroda,
                               'Edukacja': edukacja,
                               'Pozostałe': pozostale,
                               'Głośność': glosnosc,
                               'Rok budowy': rok_budowy,
                               'Dostępność od': dostepnosc,
                               'Powierzchnia dodatkowa': powierzchnia_dodatkowa,
                               'Rynek': rynek,
                               'Data aktualizacji': data_aktualizacji,
                               'Link': link_do_mieszkania,
                               'Data scrapu': today}

            wszystkie_dane_mieszkan.append(dane_mieszkania)
            c.execute(
                'INSERT INTO mieszkania VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'
                '?,?)',
                [dane_mieszkania['ID'],
                 dane_mieszkania['Nazwa oferty'],
                 dane_mieszkania['Lokalizacja - województwo'],
                 dane_mieszkania['Lokalizacja - miasto'],
                 dane_mieszkania['Lokalizacja - miasto'],
                 dane_mieszkania['Cena'],
                 dane_mieszkania['Cena za metr'],
                 dane_mieszkania['Opłaty'],
                 dane_mieszkania['Powierzchnia w m2'],
                 dane_mieszkania['Nr telefonu 1'],
                 dane_mieszkania['Nr telefonu 2'],
                 dane_mieszkania['Piętro'],
                 dane_mieszkania['Liczba pięter w budynku'],
                 dane_mieszkania['Liczba pokoi'],
                 dane_mieszkania['Forma kuchni'],
                 dane_mieszkania['Forma własności'],
                 dane_mieszkania['Stan'],
                 dane_mieszkania['Stan instalacji'],
                 dane_mieszkania['Typ zabudowy'],
                 dane_mieszkania['Materiał budynku'],
                 dane_mieszkania['Okna'],
                 dane_mieszkania['Miejsca parkingowe'],
                 dane_mieszkania['Usytuowanie (strony świata)'],
                 dane_mieszkania['Komunikacja'],
                 dane_mieszkania['Zdrowie i uroda'],
                 dane_mieszkania['Edukacja'],
                 dane_mieszkania['Pozostałe'],
                 dane_mieszkania['Głośność'],
                 dane_mieszkania['Rok budowy'],
                 dane_mieszkania['Dostępność od'],
                 dane_mieszkania['Powierzchnia dodatkowa'],
                 dane_mieszkania['Rynek'],
                 dane_mieszkania['Data aktualizacji'],
                 dane_mieszkania['Link'],
                 dane_mieszkania['Data scrapu']])
            conn.commit()
            pbar.update(1)
        else:
            pass

conn.close()
df = pd.DataFrame(wszystkie_dane_mieszkan)
df.drop_duplicates(subset=['ID', 'Data scrapu', 'Rynek'], keep='first')
df.to_excel('wyniki' + str(today) + '.xlsx')

##haslo do aws gratka: gratka123!
