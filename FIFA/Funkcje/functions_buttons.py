from time import sleep

from FIFA.Funkcje import functions_checkers


def close_button(driver):
    while not functions_checkers.elementExistsByClassName(driver, 'icon_close'):
        sleep(0.01)
        try:
            driver.find_element_by_class_name('icon_close').click()
            print('klikniete X', end="\r")
        except:
            sleep(0.2)
            print('nie klikniete', end="\r")
            pass


def back_button(driver):
    while not functions_checkers.elementExistsByClassName(driver, 'ut-navigation-button-control'[0]):
        try:
            driver.find_elements_by_class_name('ut-navigation-button-control')[0].click()
        except:
            sleep(0.2)
            print('brak przycisku, sprawdzam jeszcze raz', end="\r")


def price_fill(self, price):
    price_window = self.driver.find_elements_by_class_name('ut-numeric-input-spinner-control')[
        3].find_element_by_class_name('numericInput')

    price_window.clear()
    sleep(0.5)
    price_window.send_keys(str(price))
    sleep(1)

    print('Cena wpisana')


def auction_price_fill(self, price):
    price_window = self.driver.find_elements_by_class_name('ut-numeric-input-spinner-control')[
        1].find_element_by_class_name('numericInput')

    price_window.clear()
    sleep(0.5)
    price_window.send_keys(str(price))
    sleep(1)

    print('Cena aukcji wpisana')


def search_button(self):
    self.driver.find_elements_by_class_name('call-to-action')[0].click()
