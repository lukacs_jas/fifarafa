def elementExistsByClassName(drv, ClassName):
    try:
        drv.find_element_by_class_name(ClassName)
        return True
    except:
        return False


def elementExistsByXPath(drv, xpath):
    try:
        drv.find_element_by_xpath(xpath)
        return True
    except:
        return False
