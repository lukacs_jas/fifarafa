from time import sleep

from FIFA.Funkcje import functions_checkers, functions_buttons


def search_for_player(self, player):
    print("Szukam " + str(player))
    player_window = self.driver.find_element_by_class_name('ut-text-input-control')
    player_window.clear()
    sleep(0.5)
    player_window.click()
    sleep(0.5)
    player_window.send_keys(str(player))
    print("Zawodnik wpisany", end="\r")
    sleep(1)
    # rozwijana lista zawodników
    self.driver.find_elements_by_class_name('playerResultsList')[0].click()
    sleep(1)
    # functions_buttons.price_fill(self,price)


def search_for_price(self, price):
    functions_buttons.price_fill(self, price)


def search_for_auction_price(self, price):
    functions_buttons.auction_price_fill(self, price)


def search_for_league(self, league):
    # lista rozwijana z ligą
    drop_down_league = self.driver.find_elements_by_class_name("ut-search-filter-control")[5]
    drop_down_league.click()
    sleep(0.5)
    zero = 0

    try:
        for i in drop_down_league.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name(
                'with-icon'):
            sleep(0.1)
            if league in i.text:
                print("Wybrana liga: " +
                      drop_down_league.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name(
                          'with-icon')[zero].text)
                drop_down_league.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name(
                    'with-icon')[zero].click()
                break
            zero = zero + 1
    except:
        pass
    sleep(0.001)


def search_for_club(self, club):
    # lista rozwijana z ligą
    drop_down_club = self.driver.find_elements_by_class_name("ut-search-filter-control")[6]
    drop_down_club.click()
    sleep(0.5)
    zero = 0

    try:
        for i in drop_down_club.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name(
                'with-icon'):
            sleep(0.1)
            if club in i.text:
                print("Wybrany klub: " +
                      drop_down_club.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name(
                          'with-icon')[zero].text)
                drop_down_club.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name(
                    'with-icon')[zero].click()
                break
            zero = zero + 1
    except:
        pass
    sleep(0.001)


def search_for_level(self, level):
    # lista rozwijana z poziomem
    drop_down_level = self.driver.find_elements_by_class_name("ut-search-filter-control")[0]
    drop_down_level.click()
    sleep(0.5)
    while functions_checkers.elementExistsByClassName(self.driver,
                                                      self.driver.find_element_by_class_name("is-open")):
        sleep(0.1)
    # wybór poziomu
    if level == "Złoty":
        drop_down_level.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name('with-icon')[
            3].click()
    elif level == "Srebrny":
        drop_down_level.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name('with-icon')[
            2].click()
    elif level == "Brązowy":
        drop_down_level.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name('with-icon')[
            1].click()
    elif level == "Specjalny":
        drop_down_level.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name('with-icon')[
            4].click()
    else:
        print("Wpisany zły poziom - wychodzę")

    print("Poziom wybrany: " + level)


def search_for_type(self, type):
    drop_down_type = self.driver.find_elements_by_class_name("ut-search-filter-control")[1]
    drop_down_type.click()
    sleep(0.5)
    zero = 0
    while functions_checkers.elementExistsByClassName(self.driver,
                                                      self.driver.find_element_by_class_name("is-open")):
        try:
            for i in drop_down_type.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name(
                    'with-icon'):
                sleep(0.1)
                if type in i.text:
                    print("Wybrany klub: " +
                          drop_down_type.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name(
                              'with-icon')[zero].text)
                    drop_down_type.find_elements_by_class_name("inline-list")[0].find_elements_by_class_name(
                        'with-icon')[zero].click()
                    break
                zero = zero + 1
        except:
            pass
        sleep(0.001)
