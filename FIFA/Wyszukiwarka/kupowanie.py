from re import sub, DOTALL


def licytacja(self, cena):
    lista_zawodnikow = self.driver.find_element_by_class_name('paginated-item-list') \
        .find_elements_by_class_name('listFUTItem')

    panel_boczny = self.driver.find_element_by_class_name('DetailPanel')

    def podbicie_ceny():
        increase_price_button = \
        panel_boczny.find_element_by_class_name('ut-numeric-input-spinner-control').find_elements_by_class_name(
            'btn-standard')[1]
        increase_price_button.click()

    lista_do_bidowania = []
    lista_do_bidowania_od_zera = []
    jeden = 1
    for zawodnik in lista_zawodnikow:
        dane_zawodnika = zawodnik.find_element_by_class_name('auction')
        cena_wywolawcza = sub('\D', '', dane_zawodnika.find_elements_by_class_name('auctionValue')[0].text,
                              flags=DOTALL)
        aktualna_cena = sub('\D', '', dane_zawodnika.find_elements_by_class_name('auctionValue')[1].text, flags=DOTALL)
        kup_teraz_cena = sub('\D', '', dane_zawodnika.find_elements_by_class_name('auctionValue')[2].text, flags=DOTALL)

        if cena_wywolawcza <= cena:
            if (aktualna_cena <= cena):
                lista_do_bidowania.append(zawodnik)
            elif (aktualna_cena == ''):
                lista_do_bidowania_od_zera.append(zawodnik)
            else:
                pass
        print('Zawodnik nr:' + str(jeden) + ' sprawdzony.', end='\r')
        jeden = jeden + 1

    for zawodnik in lista_do_bidowania_od_zera:
        zawodnik.click()
        panel_boczny.find_element_by_class_name('bidButton').click()

    for zawodnik in lista_do_bidowania:
        zawodnik.click()
        podbicie_ceny()
        panel_boczny.find_element_by_class_name('bidButton').click()


def snipienie(self, cena):
    pass
