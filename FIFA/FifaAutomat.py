from time import sleep

from FIFA.Funkcje import functions_checkers
from FIFA.Funkcje.functions_buttons import search_button
from FIFA.Wchodzenie_do_webapp import Start, Login
from FIFA.Wyszukiwarka import metody, kupowanie


class Fifa20:

    def __init__(self):
        Start.start(self)
        Login.login(self)
        Login.sign_ea(self)
        Login.transfery(self)

    def search(self, **metoda):
        for key, value in metoda.items():
            if key == 'zawodnik':
                metody.search_for_player(self, player=value)
            elif key == 'cena_licytacji':
                metody.search_for_price_auction(self, price=value)
            elif key == 'cena_maksymalna':
                metody.search_for_price(self, price=value)
            elif key == 'liga':
                metody.search_for_league(self, league=value)
            elif key == 'level':
                metody.search_for_level(self, level=value)
            elif key == 'klub':
                metody.search_for_club(self, club=value)
            elif key == 'typ':
                metody.search_for_type(self, type=value)
            else:
                print('zabraklo argumentow do szukania')
        search_button(self)

    def kupno(self, **metoda):
        for key, value in metoda.items():
            if key == 'licytacja':
                kupowanie.licytacja(self, cena=value)
            elif key == 'snipienie':
                kupowanie.snipienie(self, cena=value)

    ##################################################################################3
    def klub(self):
        while not functions_checkers.elementExistsByClassName(self.driver, 'icon-club'):
            sleep(0.5)
        sleep(5)
        self.driver.find_elements_by_class_name('icon-club')[0].click()
        sleep(1)
        self.driver.find_element_by_class_name('tileContent').click()

    def zawartosc_klubu(self):
        lista_zawodnikow = []
        zawodnik = []
        for zawodnik_z_listy in self.driver.find_elements_by_class_name('listFUTItem'):
            # klik w zawodnika
            print('klikam w zawodnika')
            zawodnik_z_listy.find_element_by_class_name('ut-item-loader').click()
            # sprawdzenie czy jest wymienny czy nie
            for wymienny in self.driver.find_elements_by_class_name('btn-text'):
                if 'Wystaw' in wymienny.text:
                    # klik w dane zawodnika
                    self.driver.find_element_by_class_name('DetailPanel').find_element_by_class_name('more').click()
                    for i in self.driver.find_element_by_class_name('menu-container').find_elements_by_class_name(
                            'tab-menu-item'):
                        if 'INFORMACJE' in i.text:
                            i.click()
                            # imie i nazwisko
                            imie_nazwisko = \
                            self.driver.find_elements_by_class_name('pseudo-table')[0].text.split("""\n""")[3]
                            zawodnik.append(imie_nazwisko)
                        elif 'ATRYBUTY' in i.text:
                            i.click()
                            # ocena ogolna
                            ocena = self.driver.find_elements_by_class_name('pseudo-table')[0].text.split("""\n""")[1]
                            zawodnik.append(ocena)
                    lista_zawodnikow.append(zawodnik)
