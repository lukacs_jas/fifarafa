from time import sleep

import hasla
from FIFA.Funkcje import functions_checkers

for index in hasla.hasla:
    for page, passes in index.items():
        if page == 'Fifa':
            hasla_login_fifa = passes[0]
            hasla_pass_fifa = passes[1]
        if page == 'Poczta':
            hasla_login_poczta = passes[0]
            hasla_pass_poczta = passes[1]


def login(self):
    # klikanie logowania
    sleep(5)
    try:
        if "CENTRALA" in self.driver.find_element_by_class_name('navbar-style-landscape'):
            print('Juz zalogowany')
    except:
        while not functions_checkers.elementExistsByClassName(self.driver, 'call-to-action'):
            sleep(0.5)
        sleep(5)
        try:
            self.driver.find_element_by_class_name('call-to-action').click()
        except:
            print('coś się stało (?) : może trzeba wpisać hasło albo już jesteś zalogowany')
    print('Wchodzimy do webapp')


def sign_ea(self):
    ##todo kontrola opóźnień
    try:
        if self.driver.find_elements_by_class_name('panel-content')[0].text.split(' ')[0] in 'Zaloguj':
            print('Dodatkowa autentykacja EA')
            xpath = '/html/body/div[2]/form/div[3]/div[1]/div[1]/div/ul[1]'
            login = self.driver.find_element_by_xpath(xpath + '/li[1]/div/span[1]/span/input')
            login.clear()
            sleep(0.2)
            login.send_keys(str(hasla_login_fifa))

            password = self.driver.find_element_by_xpath(xpath + '/li[2]/div/span[1]/span/input')
            password.clear()
            sleep(0.2)
            password.send_keys(str(hasla_pass_fifa))

            if self.driver.find_element_by_id('btnLogin').text == 'Zaloguj się':
                self.driver.find_element_by_id('btnLogin').click()
            else:
                print('Coś poszło nie tak na etapie wpisywania loginu i hasłą')
    except:
        pass
        try:
            if self.driver.find_element_by_class_name('tfa-login-panel-container-style').text.find(
                    'lu****@interia.pl') == -1:
                self.driver.find_element_by_id('btnSendCode').click()

            self.driver.execute_script("window.open('');")
            self.driver.switch_to.window(self.driver.window_handles[1])
            self.driver.get('https://poczta.interia.pl')
            self.driver.find_element_by_xpath('//*[@id="formEmail"]').send_keys(str(hasla_login_poczta))
            self.driver.find_element_by_xpath('//*[@id="formPassword"]').send_keys(str(hasla_pass_poczta))
            self.driver.find_element_by_xpath('//*[@id="formSubmit"]').click()

            kod_ea = ''
            pierwszy = True
            for i in self.driver.find_elements_by_class_name('msglist-item-message-subject-text') and pierwszy == True:
                if 'Twój kod bezpieczeństwa EA' in i.text:
                    print(i.text)
                    kod_ea = i.text.split(':')[1].replace(' ', '')
                    pierwszy = False
            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[0])
            self.driver.find_element_by_xpath('//*[@id="oneTimeCode"]').clear()
            self.driver.find_element_by_xpath('//*[@id="oneTimeCode"]').send_keys(str(kod_ea))
            self.driver.find_element_by_id('btnSubmit').click()
        except:
            pass
    print('Jesteśmy zalogowani')


def transfery(self):
    while not functions_checkers.elementExistsByClassName(self.driver, 'icon-transfer'):
        sleep(0.5)
    sleep(5)
    self.driver.find_elements_by_class_name('icon-transfer')[0].click()
    sleep(1)
    self.driver.find_element_by_class_name('tileContent').click()
