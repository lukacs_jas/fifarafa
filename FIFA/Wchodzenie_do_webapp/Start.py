import getpass
import os

from selenium import webdriver

path = os.getcwd() + '/'

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('log-level=3')
chrome_options.add_argument('--allow-running-insecure-content')
chrome_options.add_argument('--disable-extensions')
chrome_options.add_argument('--profile-directory=Default')
# chrome_options.add_argument("--incognito")
# chrome_options.add_argument("--headless")
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--disable-plugins-discovery")
chrome_options.add_argument(
    "--user-data-dir=C:\\Users\\" + str(getpass.getuser()) + "\\AppData\\Local\\Google\\Chrome\\User Data\\Default")


def start(self):
    self.driver = webdriver.Chrome(options=chrome_options, executable_path=path + "/chromedriver.exe")
    self.driver.get('https://www.easports.com/pl/fifa/ultimate-team/web-app/')
    print('Przeglądarka uruchomiona')
