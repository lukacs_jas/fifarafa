import getpass
import os

from selenium import webdriver

path = os.getcwd() + '/'

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('log-level=3')
chrome_options.add_argument('--allow-running-insecure-content')
chrome_options.add_argument('--disable-extensions')
chrome_options.add_argument('--profile-directory=Default')
# chrome_options.add_argument("--incognito")
# chrome_options.add_argument("--headless")
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--disable-plugins-discovery")
chrome_options.add_argument(
    "--user-data-dir=C:\\Users\\" + str(getpass.getuser()) + "\\AppData\\Local\\Google\\Chrome\\User Data\\Default")

katalog_stron = {'Fifa': 'https://www.easports.com/pl/fifa/ultimate-team/web-app/',
                 'Poczta_interia': 'https://www.poczta.interia.pl',
                 'Mbank': 'https://online.mbank.pl/pl/Login'}


class Przegladarka:

    def webpage(self,page):
        for strona,adres in katalog_stron.items():
            if strona == page:
                adres_docelowy = adres
        self.driver.get(adres_docelowy)

    def __init__(self):
        self.driver = webdriver.Chrome(options=chrome_options, executable_path=path + "/chromedriver.exe")

