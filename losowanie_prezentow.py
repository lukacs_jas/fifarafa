def wysylka_maila(odbiorca, obdarowany):
    import smtplib, ssl
    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    from email.header import Header

    sender_email = "swiety.mikolaj.z.ozarowa@gmail.com"
    receiver_email = odbiorca
    password = 'KSOkso123'

    message = MIMEMultipart("alternative")
    message["Subject"] = Header("Pomożesz mi ze świątecznymi prezentami?", 'utf-8')
    message["From"] = Header('Święty Mikołaj z Ożarowa', 'utf-8')
    message["To"] = receiver_email

    # Create the plain-text and HTML version of your message
    html = """\
    <html>
      <body>
        <p>HO HO HO!</p>
        <p>Święta tuż tuż!</p>
        <p>Moje elfy są na kwarantannie, więc potrzebuję Ciebie, mój Drogi Pomocniku!</p>
        <p>Będę bardzo wdzięczny, jeśli przygotujesz prezent dla {}.</p>
        <p>Standardowy koszt produkcji jednego prezentu wynosi 150zł. Mam nadzieję że uda Ci się coś znaleźć do tej sumy.</p>
        <p>Liczę na Twoją pomoc,</p>
        <p>Święty Mikołaj</p>
      </body>
    </html>
    """.format(obdarowany)

    part2 = MIMEText(html, "html")
    message.attach(part2)

    # Create secure connection with server and send email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )


def losowanie():
    from random import randint
    from time import sleep
    gosia = dict(mail='malgorzata.pietruczuk@op.pl', imie='Gosi', rodzina='Jeże', dopasowanie=False, a='Gosia')
    lukasz = dict(mail='lukasz.jasinski7@interia.pl', imie='Łukasza', rodzina='Jeże', dopasowanie=False, a='Łukasz')
    ula = dict(mail='ula.jasi@interia.pl', imie='Uli', rodzina='Jasińscy', dopasowanie=False, a='Ula')
    romek = dict(mail='roman.jasinski@interia.pl', imie='Romana', rodzina='Jasińscy', dopasowanie=False, a='Romek')
    kuba = dict(mail='kubers74@gmail.com', imie='Kuby', rodzina='Jasińscy', dopasowanie=False, a='Kuba')
    ewa = dict(mail='ewa.lubanska123@gmail.com', imie='Ewy', rodzina='Lubańscy', dopasowanie=False, a='Ewa')
    mateusz = dict(mail='mateuszroman.lubanski@gmail.com', imie='Mateusza', rodzina='Lubańscy', dopasowanie=False,
                   a='Mateusz')
    agnieszka = dict(mail='a.lubanska@lagardere-tr.pl', imie='Agnieszki', rodzina='Lubańscy', dopasowanie=False,
                     a='Agnieszka')
    tomek = dict(mail='luban555@wp.pl', imie='Tomka', rodzina='Lubańscy', dopasowanie=False, a='Tomek')
    majka = dict(mail='maja.marianna.jasinska@gmail.com', imie='Mai', rodzina='Jeże', dopasowanie=False, a='Maja')

    dawca = [gosia, lukasz, ula, romek, kuba, agnieszka, mateusz, ewa, tomek, majka]
    biorca = [gosia, lukasz, ula, romek, kuba, agnieszka, mateusz, ewa, tomek, majka]

    pary = []
    wyjscie = False
    print('Witam na losowaniu! Do dzieła :)')
    sleep(2)
    for i in dawca:
        if wyjscie == False:
            licznik = 0
            while i['dopasowanie'] == False:
                potencjalny_biorca = biorca[randint(0, len(biorca) - 1)]
                licznik = licznik + 1
                if potencjalny_biorca['rodzina'] != i['rodzina']:
                    para = (i, potencjalny_biorca)
                    pary.append(para)
                    i['dopasowanie'] = True
                    biorca.remove(potencjalny_biorca)
                    print('Dopasowany: ' + str(i['a']))
                    sleep(1)
                else:
                    if licznik == 10:
                        wyjscie = True
                        break
                    else:
                        print('Losujemy jeszcze raz', end='\r')
                        # print(str(len(biorca)))
                    sleep(1)


        else:
            print('Trzeba jeszcze raz')
            break

    if wyjscie == False:
        print('Zapis do pliku')
        fo = open("dopasowania_na_swieta.txt", "w", encoding="utf-8")
        for i in pary:
            pierwsza = str(i[0]['a'])
            druga = str(i[1]['a'])
            seq = """{} i {}\n""".format(pierwsza, druga)
            fo.writelines(seq)
        fo.close()

        for i in pary:
            wysylka_maila(i[0]['mail'], i[1]['imie'])
            print('Mail wyslany do: ' + i[0]['imie'])
