import email
import getpass
import imaplib
import os
import re
from time import sleep

import hasla

hasla_login = ''
hasla_pass = ''
for index in hasla.hasla:
    for page, passes in index.items():
        if page == 'Poczta_gmail':
            hasla_login = passes[0]
            hasla_pass = passes[1]

imap_host = 'imap.gmail.com'
imap_user = hasla_login
imap_pass = hasla_pass
imap_port = 993


class Gmail:

    def gmail_connection(self):
        status = True
        self.connection = imaplib.IMAP4_SSL(imap_host, imap_port)
        while (status):
            try:
                self.connection.login(imap_user, imap_pass)
                if self.connection.state == 'NOAUTH':
                    print('Brak autoryzacji')
                else:
                    self.connection.select("Interia")
                    if self.connection.state == 'SELECTED':
                        print('Logged in')
                        status = False
            except:
                sleep(10)
                print('Ponawiam logowanie')

    def mail_content(self, message):
        print('dziala')
        if message.is_multipart():
            for part in message.walk():
                print(part)
                if part.get_content_type() == 'text/plain':
                    print('tu powinien byc tekst maila')
                if part.get_content_type() == 'application/pdf':
                    print("znalazłem pdf")
                    filename = part.get_filename()
                    filepath = os.path.join('/Users/' + str(getpass.getuser()) + '/Documents/Faktury/', filename)
                    downloader = open(filepath, 'wb')
                    downloader.write(part.get_payload(decode=True))
                    downloader.close()
        else:
            message.get_payload()

    def get_mail(self):
        mail_list = []
        state, messages = self.connection.search(None, "(UNSEEN)")
        if state == 'OK':
            for num in messages[0].split():
                typ, data = self.connection.fetch(num, '(RFC822)')
                message = email.message_from_bytes(data[0][1])
                mail_list.append(message)
        print(len(mail_list))
        for mail in mail_list:
            print(mail['From'])
            if re.search('^\S+', mail['From']).group() == 'STANSAT':
                self.mail_content(mail)

    def __init__(self):
        self.gmail_connection()
        self.get_mail()
